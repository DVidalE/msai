from peewee import *
from CfgBot import ConfigurationBot


def readBdConfig():
    cfg = ConfigurationBot.getInstance()
    bdCfg = cfg.getBdConfig()
    db = PostgresqlDatabase('english_dojo',
                            **{'host': bdCfg['host'], 'user': bdCfg['user'],
                               'password': bdCfg['password']})
    return db


database = readBdConfig()


class UnknownField(object):
    def __init__(self, *_, **__): pass


class BaseModel(Model):
    class Meta:
        database = database


class Course(BaseModel):
    course_id = AutoField()
    course_level = CharField(null=True)
    course_name = CharField(null=True)
    description = CharField(null=True)
    end_date = DateField(null=True)
    start_date = DateField(null=True)

    class Meta:
        table_name = 'course'


class Student(BaseModel):
    native_language = CharField(null=True)
    status = CharField(null=True)
    student_first_name = CharField(null=True)
    student_id = AutoField()
    student_last_name = CharField(null=True)
    sumo_coins = IntegerField(null=True)
    telegram_id = BigIntegerField(null=True)
    telegram_username = CharField(null=True)

    class Meta:
        table_name = 'student'


class CourseEnrollment(BaseModel):
    course = ForeignKeyField(column_name='course_id', field='course_id', model=Course)
    score = IntegerField(null=True)
    status = CharField(null=True)
    student = ForeignKeyField(column_name='student_id', field='student_id', model=Student)

    class Meta:
        table_name = 'course_enrollment'
        indexes = (
            (('course', 'student'), True),
        )
        primary_key = CompositeKey('course', 'student')


class Instructor(BaseModel):
    instructor_id = AutoField()
    instructor_name = CharField(null=True)
    name_alias = CharField(null=True)
    telegram_id = CharField(null=True)

    class Meta:
        table_name = 'instructor'


class CourseInstructor(BaseModel):
    course = ForeignKeyField(column_name='course_id', field='course_id', model=Course)
    instructor = ForeignKeyField(column_name='instructor_id', field='instructor_id', model=Instructor)
    status = CharField(null=True)

    class Meta:
        table_name = 'course_instructor'
        indexes = (
            (('instructor', 'course'), True),
        )
        primary_key = CompositeKey('course', 'instructor')


class StudentAnnouncement(BaseModel):
    announcement_id = AutoField()
    announcement_text = CharField(null=True)
    end_date = DateField(null=True)
    start_date = DateField(null=True)
    status = CharField(null=True)
    student = ForeignKeyField(column_name='student_id', field='student_id', model=Student, null=True)

    class Meta:
        table_name = 'student_announcement'


class Exam(BaseModel):
    description = CharField(null=True)
    exam_code = CharField(null=True)
    exam_id = AutoField()
    exam_language = CharField(null=True)
    exam_level = CharField(null=True)
    exam_type = CharField(null=True)
    instructions = CharField(null=True)

    class Meta:
        table_name = 'exam'


class StudentExam(BaseModel):
    end_date = DateTimeField(null=True)
    exam = ForeignKeyField(column_name='exam_id', field='exam_id', model=Exam, null=True)
    score = FloatField(null=True)
    stage = CharField(null=True)
    start_date = DateTimeField(null=True)
    status = CharField(null=True)
    student_exam_id = AutoField()
    student = ForeignKeyField(column_name='student_id', field='student_id', model=Student, null=True)

    class Meta:
        table_name = 'student_exam'


class ExamQuestion(BaseModel):
    description = CharField(null=True)
    exam = ForeignKeyField(column_name='exam_id', field='exam_id', model=Exam, null=True)
    question_id = AutoField()
    question_name = CharField(null=True)

    class Meta:
        table_name = 'exam_question'


class StudentExamAnswer(BaseModel):
    question = ForeignKeyField(column_name='question_id', field='question_id', model=ExamQuestion, null=True)
    score = IntegerField(null=True)
    status = CharField(null=True)
    student_answer = CharField(null=True)
    student_exam_answer_id = AutoField()
    student_exam = ForeignKeyField(column_name='student_exam_id', field='student_exam_id', model=StudentExam, null=True)

    class Meta:
        table_name = 'student_exam_answer'


class QuestionAnswer(BaseModel):
    description = CharField(null=True)
    explanation = CharField(null=True)
    question_answer_id = AutoField()
    question = ForeignKeyField(column_name='question_id', field='question_id', model=ExamQuestion, null=True)

    class Meta:
        table_name = 'question_answer'


class QuestionOption(BaseModel):
    description = CharField(null=True)
    option_name = CharField(null=True)
    question = ForeignKeyField(column_name='question_id', field='question_id', model=ExamQuestion, null=True)
    question_option_id = AutoField()

    class Meta:
        table_name = 'question_option'
