from slice_data import *

class CompositeClassifier:
    def __init__(self,classifiers,slice_method,params,x,y):
        self.classifiers= classifiers
        self.slicer = SlicerFactory.makeSlicer(x, y, slice_method, params)


    def add_classifier(self,classifier):
        self.classifiers.add(classifier)

    def remove_classifier(self,classifier):
        self.classifiers.remove(classifier)

    def fit(self):
        for x_train,y_train in self.slicer.train():
            for classifier in self.classifiers:
                classifier.fit(x_train,y_train)

    def predict(self):
        for x_test, y_test in self.slicer.test():
            list_preds = np.zeros((len(self.classifiers),y_test.shape[0]))
            for idx,classifier in enumerate(self.classifiers):
                list_preds[idx]= classifier.predict(x_test)

            final_pred= self.combine_predictions(list_preds)
            return (y_test,final_pred)


    def combine_predictions(self,list_preds):
        final_preds = np.zeros(list_preds.shape[1])
        for i in range(list_preds.shape[1]):
            values,counts= np.unique(list_preds[:,i], return_counts=True)
            final_preds[i]=values[np.argmax(counts)]
        return final_preds


