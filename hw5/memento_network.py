import json
from datetime import datetime

class MementoNeural:
    def __init__(self,state,path):
        self.file_state= f'{path}/state_{datetime.now().strftime("%d-%m-%Y_%H-%M-%S")}.json'
        with open(self.file_state, "w") as write_file:
            json.dump(state, write_file)

    def get_state(self):

        with open(self.file_state, "r") as read_file:
            return(json.load(read_file))
