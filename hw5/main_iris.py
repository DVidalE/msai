from sklearn.datasets import load_iris
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from slice_data import *
from ensemble_classifiers import CompositeClassifier
from sklearn.metrics import accuracy_score
from neural_network import *

def main():
    data = load_iris()
    #train data with one classifier but multiple slicing strategies
    singleClassifier(data)
    multipleClassifier(data)
    train_neural()


def singleClassifier(data):
    x,y= data.data,data.target
    svm= SVC(kernel = 'linear', C = 1)
    test_holdout(x, y, svm)
    test_kfold(x, y, svm)

def multipleClassifier(data):
    x, y = data.data, data.target
    params = {'percentage': 0.7}
    svm = SVC(kernel='linear', C=1)
    knn = KNeighborsClassifier(n_neighbors=7)
    classifiers = [svm,knn]
    ensemble = CompositeClassifier(classifiers,'holdout',params,x,y)
    ensemble.fit()
    y_test,y_pred = ensemble.predict()
    accuracy= accuracy_score(y_test, y_pred)
    print("multiclassifier")
    print(accuracy)



def test_holdout(x,y,svm):
    print("testing holdout")
    # params holdout
    params_holdout = {'percentage': 0.7}
    slicer = SlicerFactory.makeSlicer(x, y, 'holdout', params_holdout)
    for x_train, y_train, x_test, y_test in slicer.step():
        model = svm.fit(x_train, y_train)
        acc = model.score(x_test, y_test)
        print(acc)

def test_kfold(x,y,svm):
    print("testing kfold")
    params_kfold = {'k_fold':5}
    slicer = SlicerFactory.makeSlicer(x,y,'kfold',params_kfold)
    list_models= []
    for x_train, y_train, x_test, y_test in slicer.step():
        model = svm.fit(x_train, y_train)
        acc = model.score(x_test, y_test)
        print(acc)
        list_models.append(model)

if __name__ == "__main__":
    main()