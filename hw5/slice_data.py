
from sklearn.utils import shuffle
import numpy as np

class Holdout:
    def __init__(self,x,y,percentage=0.7):
        self.x,self.y = shuffle(x,y)
        self.split = int(self.x.shape[0] * percentage)

    def train(self):
        yield self.x[0:self.split], self.y[0:self.split]

    def test(self):
        yield self.x[self.split:], self.y[self.split:]


    def step(self):
        for i in range(1):
            yield self.x[0:self.split], self.y[0:self.split], self.x[self.split:], self.y[self.split:]

class KFold:
    def __init__(self,x,y,n_folds=5):
        self.x, self.y = shuffle(x, y)
        self.partitions = [int(lim) for lim in np.linspace(0,self.x.shape[0],num=n_folds).tolist()]

    def step(self):

        for lim_inf,lim_sup in zip(self.partitions[0:-1],self.partitions[1:]):
            if lim_inf ==0:
                x_train,y_train = self.x[lim_sup:],self.y[lim_sup:]
            if lim_sup == self.x.shape[0]:
                x_train,y_train= self.x[0:lim_inf],self.y[0:lim_inf]
            else:
                x_train = np.concatenate((self.x[0:lim_inf-1],self.x[lim_sup:]),axis=0)
                y_train = np.concatenate((self.y[0:lim_inf-1],self.y[lim_sup:]),axis=0)

            yield x_train,y_train,self.x[lim_inf:lim_sup],self.y[lim_inf:lim_sup]


class SlicerFactory:
    @staticmethod
    def create_holdout(x,y,params):
        percentage = params['percentage'] if 'percentage' in params else 0.7
        return Holdout(x,y,percentage)

    @staticmethod
    def create_kfold(x, y, params):
        k_fold = params['k_fold'] if 'k_fold' in params else 5
        return KFold(x, y, k_fold)

    @staticmethod
    def makeSlicer(x,y,slicer_method,params):
        if slicer_method =='holdout':
            return  SlicerFactory.create_holdout(x,y,params)
        if slicer_method == 'kfold':
            return SlicerFactory.create_kfold(x,y,params)
