from datetime import datetime

from entities import StudentExam, Exam, ExamQuestion, QuestionOption, StudentExamAnswer, readBdConfig
from peewee import *
from sharedQueries import SharedQueries
from telebot import types


class AssessCommand:
    def assess(self, msgUser):
        exam = self.getStudentExam(msgUser.id)
        if exam is None:
            return {'action': 'askLanguageLevel', 'msg': self.askLanguageLevel()}

    def getStudentExam(self, user_id):
        try:
            exam = (StudentExam.select()
                    .join(Exam, on=(StudentExam.exam == Exam.exam_id))
                    .where(
                StudentExam.student == user_id
                and Exam.exam_type == 'entrance'
            ).get()

                    )
        except DoesNotExist:
            exam = None
        return exam

    def askLanguageLevel(self):
        msgText = 'What do you think is your english level according to the Common European framework?'
        markup = types.ReplyKeyboardMarkup()
        itemA1 = types.KeyboardButton('A1')
        itemB1 = types.KeyboardButton('B1')
        itemB2 = types.KeyboardButton('B2')
        markup.row(itemA1, itemB1, itemB2)
        return {'msgtext': msgText, 'markup': markup}

    def getExamKeyboard(self):
        markup = types.ReplyKeyboardMarkup()
        itemFirst = types.KeyboardButton('<<')
        itemBack = types.KeyboardButton('<')
        itemForward = types.KeyboardButton('>')
        itemLast = types.KeyboardButton('>>')
        itemESend = types.KeyboardButton('[send]')
        markup.row(itemFirst, itemBack, itemForward, itemLast)
        markup.row(itemESend)
        return markup

    def assessWithLangLevel(self, msg, user_id):

        if user_id is None:
            user = SharedQueries.getstudentbytelegramid(msg.from_user.id)
            user_id = user.student_id
        assessResponse = self.startFirstExam(msg, user_id)
        assessResponse['keyboard'] = self.getExamKeyboard()
        return assessResponse

    def getExam(self, exam_language, exam_level, exam_type):
        try:
            exam = (
                Exam.select()
                    .where(
                    Exam.exam_language == exam_language
                    and Exam.exam_level == exam_level
                    and Exam.exam_type == exam_type
                ).get()
            )
        except DoesNotExist:
            exam = None
        return exam

    def getQuestions(self, exam_id):
        try:
            questions = list()
            rsQuuery = (
                ExamQuestion.select(
                    ExamQuestion.question_id,
                    ExamQuestion.question_name,
                    ExamQuestion.description.alias('question_description')
                ).where(
                    ExamQuestion.exam == exam_id
                )
            )
            for rs in rsQuuery:
                questions.append(rs)
        except DoesNotExist:
            questions = None

        questions_options = self.getQuestionsOptions(exam_id)
        return {'questions': questions, 'question_options': questions_options}

    def getQuestionsOptions(self, exam_id):
        try:

            qOptions = dict()
            queryResult = (
                QuestionOption.select()
                    .join(ExamQuestion, on=(ExamQuestion.question_id == QuestionOption.question))
                    .where(
                    ExamQuestion.exam == exam_id
                )
            )
            for q in queryResult:
                if q.question_id in qOptions:
                    qOptions[q.question_id].append(q)
                else:

                    questionList = list()
                    questionList.append(q)
                    qOptions[q.question_id] = questionList
        except DoesNotExist:
            qOptions = None
        return qOptions

    def startFirstExam(self, msg, user_id):
        exam = self.getExam('eng', msg.text, 'entrance')
        if exam is not None:
            entrance = StudentExam(
                student=user_id,
                exam=exam.exam_id,
                stage='progress',
                start_date=datetime.now()
            )
            entrance.save()
            rsQuestions = self.getQuestions(exam.exam_id)
        else:
            entrance = None
            rsQuestions = {'questions': None, 'question_options': None}
        response = {'exam': exam, 'student_exam': entrance}
        response.update(rsQuestions)
        return response

    def submitExam(self, full_exam):
        answersToSave = list()
        questions = full_exam['questions']
        student_exam_id = full_exam['student_exam_id']
        for q in questions:
            answer = StudentExamAnswer(
                question=q.question_id,
                status='submitted',
                student_answer=q.answer,
                student_exam=student_exam_id

            )
            answersToSave.append(answer)

        # save
        db = readBdConfig()
        with db.atomic():
            StudentExamAnswer.bulk_create(answersToSave, batch_size=100)
