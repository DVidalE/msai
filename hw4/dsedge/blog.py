
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)

from dsedge.blogstorage import BlogStorage

bp = Blueprint('blog', __name__)

@bp.route('/')
def index():
    posts= BlogStorage.getInstance().get_posts()
    return render_template('index.html', posts=posts)

@bp.route('/create', methods=('GET', 'POST'))
def create():
    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if not body:
            error= 'Body is required.'

        if error is not None:
            flash(error)
        else:
            BlogStorage.getInstance().insert_post(title,body)
            return redirect(url_for('blog.index'))

    return render_template('create.html')

@bp.route('/<int:id>', methods=['GET'])
def view(id):
    post= BlogStorage.getInstance().get_post(id)
    return render_template('detail.html', post=post)