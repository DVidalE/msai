import json
from datetime import datetime

class BlogStorage:
    __instance = None

    @staticmethod
    def getInstance():
        if BlogStorage.__instance is None:
            BlogStorage.__instance = BlogStorage()

        return BlogStorage.__instance

    def __init__(self):

        with open('dsedge/blog_records.json', "r") as read_file:
            self.blogRecords = json.load(read_file)
            self.idx= max([x['id'] for x in self.blogRecords['posts']])

    def get_posts(self):
        return self.blogRecords['posts']

    def insert_post(self,title,body):
        self.idx +=1
        post={
            "id":self.idx,
            "header":title,
            "contents":body,
            "createdon":datetime.now().strftime("%Y/%m/%d %H:%M")
        }
        self.blogRecords['posts'].append(post)

    def get_post(self, id):
        return [x for x in self.blogRecords['posts'] if x['id']==id][0]