import telebot
from CfgBot import ConfigurationBot
from queryResolver import QueryResolver


class SumoBot():
    def __init__(self):
        self.configuration = ConfigurationBot.getInstance()
        self.bot = telebot.TeleBot(self.configuration.getToken(), parse_mode=None)
        self.bdEngine = QueryResolver()
        self.languageLevels = ['A1', 'B1', 'C1']
        self.user = None
        self.currentAnswer = None

        @self.bot.message_handler(commands=['start', 'help'])
        def send_welcome(message):
            self.startCommand(message)

        @self.bot.message_handler(commands=['assess'])
        def send_assessment(message):
            self.assessCommand(message)

        @self.bot.message_handler(func=lambda msg: msg.text in self.languageLevels)
        def sendLangLevel(message):
            self.assessWithLangLevel(message)

        @self.bot.message_handler(func=lambda msg: msg.text == '<<')
        def sendFirstQuestion(message):
            self.goFirstQuestion(message)

        @self.bot.message_handler(func=lambda msg: msg.text == '<')
        def sendPrevQuestion(message):
            self.goPrevQuestion(message)

        @self.bot.message_handler(func=lambda msg: msg.text == '>')
        def sendNextQuestion(message):
            self.goNextQuestion(message)

        @self.bot.message_handler(func=lambda msg: msg.text == '>>')
        def sendPrevQuestion(message):
            self.goLastQuestion(message)

        @self.bot.message_handler(func=lambda msg: msg.text == '[send]')
        def sendSubmitExam(message):
            self.submitExam(message)

        @self.bot.message_handler(func=lambda msg: msg.text != '')
        def sendDefaultMsg(message):
            self.handleDefaultMsg(message)

    def startCommand(self, message):
        loginResponse = self.bdEngine.loginUser(message.from_user)
        self.user = loginResponse['user']
        self.bot.reply_to(message, f'Hello {self.user.student_first_name}')
        for msg in loginResponse['announcements']:
            self.bot.send_message(message.chat.id, msg.announcement_text)

    def assessCommand(self, message):
        self.bot.reply_to(message, 'Welcome to the entrance quiz!!')
        assessResponse = self.bdEngine.assess(message.from_user)
        if assessResponse['action'] == 'askLanguageLevel':
            self.askLanguageLevel(message, assessResponse['msg'])

    def assessWithLangLevel(self, message):
        user_id = self.user.student_id if self.user is not None else None
        assessResponse = self.bdEngine.assessWithLangLevel(message, user_id)
        self.setExamState(assessResponse)
        keyboard = assessResponse['keyboard']
        if self.curExam is not None:
            self.bot.send_message(message.chat.id, self.curExam.instructions, reply_markup=keyboard)
        if self.questions is not None:
            self.printQuestion(message.chat.id)

    def setExamState(self, assessResponse):
        self.curExam = assessResponse['exam']
        self.questions = assessResponse['questions']
        self.questionOptions = assessResponse['question_options']
        self.currentQuestion = 0
        self.currStudentExam = assessResponse['student_exam']
        self.confirmSent = False
        self.student_exam = assessResponse['student_exam']

    def clearExamState(self):
        self.curExam = None
        self.questions = None
        self.questionOptions = None
        self.currentQuestion = None
        self.currStudentExam = None
        self.confirmSent = None
        self.student_exam = None

    def printQuestion(self, chat_id):
        self.confirmSent = False
        question = self.questions[self.currentQuestion]
        header = f'{question.question_name}. {question.question_description}'
        self.bot.send_message(chat_id, header)
        print(self.questionOptions)
        if hasattr(question, 'answer') and question.answer is not None:
            self.bot.send_message(chat_id, f'Your current answer is: {question.answer}')

        options = self.questionOptions[question.question_id]
        msg = ''
        for o in options:
            msg += f'{o.option_name}. {o.description}\n'
        self.bot.send_message(chat_id, msg)

    def setAnswer(self):
        if self.currentAnswer != '':
            self.questions[self.currentQuestion].answer = self.currentAnswer
            self.currentAnswer = ''

    def askLanguageLevel(self, message, question):
        self.bot.send_message(message.chat.id, question['msgtext'], reply_markup=question['markup'])

    def goPrevQuestion(self, message):
        if (self.currentQuestion == 0):
            self.bot.send_message(message.chat.id, 'You are already in the first question')
            return

        self.setAnswer()
        self.currentQuestion = self.currentQuestion - 1
        self.printQuestion(message.chat.id)

    def goFirstQuestion(self, message):
        if (self.currentQuestion == 0):
            self.bot.send_message(message.chat.id, 'You are already in the first question')
            return

        self.setAnswer()
        self.currentQuestion = 0
        self.printQuestion(message.chat.id)

    def goNextQuestion(self, message):
        if (self.currentQuestion == len(self.questions) - 1):
            self.bot.send_message(message.chat.id, 'You are already in the last question')
            return

        self.setAnswer()
        self.currentQuestion += 1
        self.printQuestion(message.chat.id)

    def goLastQuestion(self, message):
        if (self.currentQuestion == len(self.questions) - 1):
            self.bot.send_message(message.chat.id, 'You are already in the last question')
            return

        self.setAnswer()
        self.currentQuestion = len(self.questions) - 1
        self.printQuestion(message.chat.id)

    def submitExam(self, message):
        self.setAnswer()
        if (not self.confirmSent):
            msg = ''
            self.bot.send_message(message.chat.id, 'To confirm your answer, please push the send button again')
            for q in self.questions:
                if hasattr(q, 'answer') and q.answer is not None:
                    msg += f'Question {q.question_name}. Your answer: {q.answer}\n'
                else:
                    msg += f'Question {q.question_name}. Your answer is missing \n'
            self.confirmSent = True
            self.bot.send_message(message.chat.id, msg)
        else:
            full_exam = {'questions': self.questions, 'student_exam_id': self.student_exam.student_exam_id}
            self.bdEngine.submitExam(full_exam)
            markup = telebot.types.ReplyKeyboardRemove(selective=False)
            self.bot.send_message(message.chat.id, 'Your exam was submitted', reply_markup=markup)
            self.clearExamState()

    def handleDefaultMsg(self, message):
        if self.questions is not None:
            self.currentAnswer = message.text
        else:

            self.bot.send_message(message.chat.id, 'I cannot understand your message')

    def start(self):
        self.bot.infinity_polling()


def start():
    s = SumoBot()
    s.start()


start()
