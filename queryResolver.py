from startCommand import StartCommand
from assessCommand import AssessCommand


class QueryResolver:
    def __init__(self):
        self.startcmd = StartCommand()
        self.assesscmd = AssessCommand()

    def loginUser(self, messageUser):
        return self.startcmd.loginUser(messageUser)

    def assess(self, messageUser):
        return self.assesscmd.assess(messageUser)

    def assessWithLangLevel(self, message, user_id):
        return self.assesscmd.assessWithLangLevel(message, user_id)

    def submitExam(self, full_exam):
        self.assesscmd.submitExam(full_exam)
