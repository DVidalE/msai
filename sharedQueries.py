from entities import Student
from peewee import *


class SharedQueries:
    @staticmethod
    def getstudentbytelegramid( telegramid):

        try:
            currstudent = (Student.select()
                           .where(Student.telegram_id == telegramid)).get()
        except DoesNotExist:
            currstudent = None
        return currstudent
