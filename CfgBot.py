from dotenv import dotenv_values


class ConfigurationBot:
    __instance = None

    @staticmethod
    def getInstance():
        if ConfigurationBot.__instance is None:
            ConfigurationBot.__instance = ConfigurationBot()

        return ConfigurationBot.__instance

    def __init__(self):
        self.botConfig = dotenv_values("config.env")

    def getToken(self):
        return self.botConfig['sumo_token']

    def getBdConfig(self):
        return {'host': self.botConfig['sumo_host'],
                'user': self.botConfig['sumo_user'],
                'password': self.botConfig['sumo_pass']}
