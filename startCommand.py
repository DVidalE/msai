from datetime import date

from entities import Student, StudentAnnouncement
from sharedQueries import SharedQueries


class StartCommand:
    def loginUser(self, messageUser):
        student = SharedQueries.getstudentbytelegramid(messageUser.id)
        if student is None:
            loginResponse = self.startNewUser(messageUser)
        else:
            msgs = self.getAnnouncements(student.student_id)
            loginResponse = {'user': student, 'announcements': msgs}

        return loginResponse

    def startNewUser(self, msgUser):
        currStudent = self.createUser(msgUser)
        msgs = list()
        arrivalTxt = '''
        Welcome to English Sumo:
        To start your journey to become a black belt in English,
        I suggest  to take the entrance quiz. To
        do it, please type /assess
        '''
        welcomeMsg = self.createAnnouncement(currStudent, arrivalTxt)
        msgs.append(welcomeMsg)
        return {'user': currStudent, 'announcements': msgs}

    def createUser(self, msgUser):

        currStudent = Student(native_language=msgUser.language_code
                              , status='active', student_first_name=msgUser.first_name
                              , student_last_name=msgUser.last_name
                              , sumo_coins=1000, telegram_id=msgUser.id
                              , telegram_username=msgUser.username)
        currStudent.save()
        return currStudent

    def createAnnouncement(self, currStudent, msgTxt, startDate=date.today(), endDate=date.today()):
        msg = StudentAnnouncement(
            student=currStudent.student_id
            , announcement_text=msgTxt
            , start_date=startDate
            , end_date=endDate
            , status='active'
        )
        msg.save()
        return msg

    def getAnnouncements(self, studentId):
        msgs = list()
        rsQuery = (StudentAnnouncement.select()
                   .where(StudentAnnouncement.student == studentId
                          and StudentAnnouncement.status == 'active'
                          and StudentAnnouncement.start_date >= date.today()
                          )
                   )
        for m in rsQuery:
            msgs.append(m)
        return msgs
